import socket
import json

class Book:
    def __init__(self, name, tag, image):
        self.__name = name
        self.__tag = tag
        self.__image = image

    @property
    def name(self):
        return self.__name
    
    @property
    def image(self):
        return self.__image
    
    @property
    def tag(self):
        return self.__tag

    def __str__(self):
        return f'Book: {self.__name} ({self.__tag})'

class BookEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Book):
            return {
                'name': obj.name,
                'tag': obj.tag,
                'image': obj.image
            }
        return super().default(obj)

class Library:
    def __init__(self):
        self.__books = []

    def add_book(self, book):
        self.__books.append(book)

    def display_books(self):
        return '\n'.join(str(book) for book in self.__books) if self.__books else "La bibliothèque est vide."

    def remove_book(self, name):
        for book in self.__books:
            if book.name == name:
                self.__books.remove(book)
                return f"Livre '{name}' supprimé de la bibliothèque."
        return f"Livre '{name}' non trouvé dans la bibliothèque."

    def save(self):
        with open('bib.json', 'w') as output:
            save_str = json.dumps(self.__books, cls=BookEncoder)
            output.write(save_str)

    def load(self):
        try:
            with open('bib.json', 'r') as input:
                data = json.load(input)
                self.__books = [Book(**item) for item in data]
        except FileNotFoundError:
            self.__books = []

def tcp_server(host, port, lib):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(5)
    print(f"Server running on {host}:{port}")

    try:
        while True:
            client_socket, addr = server_socket.accept()
            with client_socket:
                print('Connected by', addr)
                data = client_socket.recv(1024)
                if not data:
                    break
                command, *args = data.decode().split()
                if command == 'ADD':
                    book = Book(*args)
                    lib.add_book(book)
                    response = f"Added {book}"
                elif command == 'REMOVE':
                    response = lib.remove_book(args[0])
                elif command == 'LIST':
                    response = lib.display_books()
                elif command == 'SAVE':
                    lib.save()
                    response = "Library saved"
                elif command == 'LOAD':
                    lib.load()
                    response = "Library loaded"
                client_socket.sendall(response.encode())
    finally:
        server_socket.close()

if __name__ == "__main__":
    lib = Library()
    host = 'localhost'
    port = 12345
    tcp_server(host, port, lib)
