import socketserver
import threading

# Define a global list to store connected clients
clients = []

class Server(socketserver.BaseRequestHandler):
    def handle(self):
        # Add client to the list
        clients.append(self.request)
        print(f"Client {self.client_address[0]} connected.")

        try:
            while True:
                # Receive data from client
                data = self.request.recv(1024).strip().decode("utf-8")
                if data:
                    print(f"Received from {self.client_address[0]}:", data)
                    # Notify all other clients
                    for client in clients:
                        if client != self.request:
                            client.sendall(bytes(data + "\n", "utf-8"))
                else:
                    break
        finally:
            
            clients.remove(self.request)
            print(f"Client {self.client_address[0]} disconnected.")

# Start the server
if __name__ == "__main__":
    HOST, PORT = "localhost", 9999
    with socketserver.TCPServer((HOST, PORT), Server) as server:
        print("Server started.")
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.start()
        server_thread.join()
