from abc import ABC, abstractmethod


class FlyBehavior(ABC):
    @abstractmethod
    def fly(self):
        pass


class Fly2Wings(FlyBehavior):
    def fly(self):
        print("Je vole avec deux ailes!")

class FlyNone(FlyBehavior):
    def fly(self):
        print("Je ne peux pas voler.")


class QuackBehavior(ABC):
    @abstractmethod
    def quack(self):
        pass


class Quack2Palmsates(QuackBehavior):
    def quack(self):
        print("Quack avec un son de palmes!")

class QuackNone(QuackBehavior):
    def quack(self):
        print("Silence complet.")


class Canard:
    def __init__(self, name, fly_behavior: FlyBehavior, quack_behavior: QuackBehavior):
        self.name = name
        self.fly_behavior = fly_behavior
        self.quack_behavior = quack_behavior

    def perform_fly(self):
        self.fly_behavior.fly()

    def perform_quack(self):
        self.quack_behavior.quack()

    def set_fly_behavior(self, new_fly_behavior: FlyBehavior):
        self.fly_behavior = new_fly_behavior

    def set_quack_behavior(self, new_quack_behavior: QuackBehavior):
        self.quack_behavior = new_quack_behavior

    def display(self):
        raise NotImplementedError("Cette méthode doit être implémentée par les sous-classes.")


class Green(Canard):
    def display(self):
        print(f"{self.name} le canard vert s'affiche à l'écran.")

class Blue(Canard):
    def display(self):
        print(f"{self.name} le canard bleu s'affiche à l'écran.")

class Grey(Canard):
    def display(self):
        print(f"{self.name} le canard gris s'affiche à l'écran.")

class Red(Canard):
    def display(self):
        print(f"{self.name} le canard rouge s'affiche à l'écran.")


if __name__ == "__main__":
    green_duck = Green("Verdi", Fly2Wings(), Quack2Palmsates())
    green_duck.perform_fly()
    green_duck.perform_quack()
    green_duck.display()

    # Change le comportement de vol en temps réel
    green_duck.set_fly_behavior(FlyNone())
    green_duck.perform_fly()  # Affichera "Je ne peux pas voler."
