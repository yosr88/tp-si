from abc import ABC, abstractmethod

# Observer Interface
class Observer(ABC):
    @abstractmethod
    def update(self, temperature, humidity, pressure):
        pass

# Subject Interface
class Subject(ABC):
    @abstractmethod
    def register_observer(self, observer: Observer):
        pass

    @abstractmethod
    def remove_observer(self, observer: Observer):
        pass

    @abstractmethod
    def notify_observers(self):
        pass

# WeatherData now acts as the subject
class WeatherData(Subject):
    def __init__(self):
        self.observers = []
        self.temperature = 0
        self.humidity = 0
        self.pressure = 0

    def register_observer(self, observer: Observer):
        self.observers.append(observer)

    def remove_observer(self, observer: Observer):
        self.observers.remove(observer)

    def notify_observers(self):
        for observer in self.observers:
            observer.update(self.temperature, self.humidity, self.pressure)

    def measurements_changed(self):
        self.notify_observers()

    # Rest of WeatherData's methods unchanged

# Display classes now implement Observer
class TempDisplay(Observer):
    def update(self, temperature, humidity, pressure):
        print(f"Température mise à jour : {temperature}°C")

class HumidityDisplay(Observer):
    def update(self, temperature, humidity, pressure):
        print(f"Humidité mise à jour : {humidity}%")

class PressureDisplay(Observer):
    def update(self, temperature, humidity, pressure):
        print(f"Pression mise à jour : {pressure} Pa")

# Example usage
weather_data = WeatherData()
temp_display = TempDisplay()
humidity_display = HumidityDisplay()
pressure_display = PressureDisplay()

weather_data.register_observer(temp_display)
weather_data.register_observer(humidity_display)
weather_data.register_observer(pressure_display)

weather_data.set_measurements(25, 65, 1013)  # Les observateurs sont notifiés
