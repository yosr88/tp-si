class Book:
    def __init__(self, author, title, content=None):
        self.author = author
        self.title = title
        self.content = content  

    def update(self, author=None, title=None, content=None):
        self.author = author if author else self.author
        self.title = title if title else self.title
        self.content = content if content is not None else self.content

    def display(self):
        print(f"Author: {self.author}, Title: {self.title}, Content: {self.content if self.content else 'Contenu indisponible'}")

class Library:
    def __init__(self):
        self.books = []

    def add_book(self, book):
        if any(b.title == book.title and b.author == book.author for b in self.books):
            print("TCe livre existe déjà")
            return
        self.books.append(book)
        print(f"Livre ajouté: {book.title} by {book.author}")

    def remove_book(self, title):
        for book in self.books:
            if book.title == title:
                self.books.remove(book)
                print(f"Livre supprimé: {title}")
                return
        print("Livre introuvable.")

    def find_book(self, title):
        for book in self.books:
            if book.title == title:
                return book
        print("Livre introuvable.")
        return None


lib = Library()
book = Book("George Orwell", "1984", "Dystopian novel")
lib.add_book(book)
found_book = lib.find_book("1984")
if found_book:
    found_book.display()
found_book.update(content="Updated dystopian novel")
found_book.display()
lib.remove_book("1984")
