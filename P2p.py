import socket
import sys
import threading

HOST, PORT = "localhost", 9999

def receive_data(sock):
    while True:
        received = sock.recv(1024).strip().decode("utf-8")
        if received:
            print("Received:", received)

def send_data(sock):
    while True:
        data = input("Enter message: ")
        sock.sendall(bytes(data + "\n", "utf-8"))

# Create a socket (SOCK_STREAM means a TCP socket)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((HOST, PORT))
    # Start a thread to receive data from server
    receive_thread = threading.Thread(target=receive_data, args=(sock,))
    receive_thread.start()
    # Send data to server
    send_data(sock)
