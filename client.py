import socket

def tcp_client(server_host, server_port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((server_host, server_port))  # Connectez-vous à l'adresse du serveur
        print(f"Connected to {server_host} on port {server_port}")

        while True:
            message = input("Enter command (or type 'exit' to quit): ")
            if message.lower() == 'exit':
                break  # Sortez de la boucle si l'utilisateur tape 'exit'

            sock.sendall(message.encode())  # Envoyez la commande encodée au serveur
            response = sock.recv(1024)  # Recevez la réponse du serveur
            print("Response from server:", response.decode())  # Affichez la réponse

if __name__ == "__main__":
    server_host = 'localhost'  # L'adresse IP du serveur
    port = 12345  # Le port sur lequel le serveur écoute
    tcp_client(server_host, port)  # Démarrez le client TCP
